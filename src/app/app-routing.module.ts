import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { InternalComponent } from './errors/internal/internal.component';

const routes: Routes = [
  {
    loadChildren: './modules/auth/auth.module#AuthModule',
    path: 'auth'
  },
  {
    loadChildren: './modules/scrum/scrum.module#ScrumModule',
    path: 'projects'
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/projects',
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '500',
    component: InternalComponent,
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
