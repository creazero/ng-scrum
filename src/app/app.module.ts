import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TokenInterceptor } from './services/token.interceptor';
import { MAT_DATE_LOCALE } from '@angular/material';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { InternalComponent } from './errors/internal/internal.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    InternalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'ru-RU'
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
