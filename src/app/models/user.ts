export interface User {
  id?: number;
  avatar: string; // url
  createdAt: Date;
  token?: string;
  username: string;
  role?: string;
}
