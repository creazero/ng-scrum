import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Текущий пользователь
   */
  private user: User;

  constructor(private http: HttpClient,
              private router: Router) { }

  get token(): string {
    return localStorage.getItem('token');
  }

  /**
   * Получаем информацию о текущем пользователе
   */
  getUser(): Observable<User> {
    if (this.user) {
      return of(this.user);
    }
    return this.getUserInfo();
  }

  /**
   * Проверяем авторизован ли пользователь
   */
  isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  /**
   * Функция запроса на авторизацию пользователя в системе
   * @param username имя пользователя
   * @param password пароль
   */
  login(username: string, password: string): Observable<User> {
    return this.http.post<User>(environment.endpoint + '/login', { username, password })
      .pipe(
        tap(user => {
          localStorage.setItem('token', user.token);
          this.user = user;
        }),
        catchError((err: HttpResponse<User>) => this.handleError(err.status))
      );
  }

  /**
   * Выходим из системы
   */
  logout(): void {
    localStorage.removeItem('token');
  }

  signUp(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${environment.endpoint}/users`, { username, password }).pipe(
      catchError((err: HttpResponse<User>) => this.handleError(err.status))
    );
  }

  /**
   * Получаем информацию о текущем пользователе из jwt
   */
  private getUserInfo(): Observable<User> {
    return this.http.get<User>(environment.endpoint + '/current_user').pipe(
      tap(user => this.user = user),
      catchError((err: HttpResponse<User>) => this.handleError(err.status))
    );
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }
}
