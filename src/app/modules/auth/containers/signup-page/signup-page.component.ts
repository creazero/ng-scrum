import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css']
})
export class SignupPageComponent implements OnInit {

  public form: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  public onSubmit(): void {
    this.authService.signUp(
      this.form.value.username,
      this.form.value.password,
    ).subscribe(user => {
      if (user) {
        this.router.navigate(['/auth/login']);
      }
    });
  }

  private initForm(): void {
    this.form = this.fb.group({
      username: this.fb.control(
        '',
        [Validators.required, Validators.minLength(3)]
      ),
      password: this.fb.control(
        '',
        [Validators.required, Validators.minLength(6)]
      ),
      passwordRepeat: this.fb.control(
        '',
        [Validators.required, Validators.minLength(6)]
      )
    }, {
      validators: [this.matchValidator.bind(this)]
    });
  }

  private matchValidator(group: FormGroup): ValidationErrors {
    const valid = group.controls.password.value === group.controls.passwordRepeat.value;

    if (!valid) {
      group.controls.passwordRepeat.setErrors({ mismatch: true });
    }

    return valid ? null : { mismatch: true};
  }
}
