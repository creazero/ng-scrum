import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserInfo } from '../../../../models/user-info';
import { AuthService } from '../../../../services/auth.service';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.initForm();
  }

  /**
   * Обрабатываем событие сабмита формы
   */
  onSubmit() {
    if (this.loginForm.invalid) { return; }
    const userInfo = this.loginForm.value as UserInfo;
    this.authService.login(userInfo.username, userInfo.password)
      .subscribe(user => {
        if (!user) {
          this.loginForm.get('password').setErrors({ invalidPassword: true });
        } else {
          this.router.navigate(['/projects']);
        }
      });
  }

  /**
   * Инициализируем форму логина
   */
  private initForm(): void {
    this.loginForm = this.fb.group({
      username: [
        '',
        [
          Validators.required,
        ]
      ],
      password: [
        '',
        [
          Validators.required
        ]
      ],
    });
  }
}
