import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';

import { User } from '../../../models/user';
import { environment } from '../../../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient,
              private router: Router) { }

  public getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.endpoint}/users`).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  public getProjectUsers(projectId: string): Observable<User[]> {
    return this.http.get<User[]>(
      `${environment.endpoint}/users`,
      { params: { project_id: projectId }}).pipe(
        catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }
}
