import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';

import { Task } from '../models/task';
import { environment } from '../../../../environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { Board } from '../models/board';
import { User } from '../../../models/user';

@Injectable()
export class TaskService {

  constructor(private http: HttpClient,
              private router: Router) {
  }

  assign(task: Task, userId: number): Observable<User> {
    return this.http.post<User>(
      `${environment.endpoint}/tasks/assign`,
      {taskId: task.id, userId}).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  deleteTask(taskId: number): Observable<boolean> {
    return this.http.delete(
      `${environment.endpoint}/tasks/${taskId}`, {observe: 'response'}).pipe(
      map(response => response.status === 200),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getTask(taskId: string): Observable<Task> {
    return this.http.get<Task>(`${environment.endpoint}/tasks/${taskId}`).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  /**
   * Retrieve a list of tasks from the project with id
   * @param projectId a project id
   */
  getTasksFromProject(projectId: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${environment.endpoint}/tasks`, {
      params: {project_id: projectId}
    }).pipe(catchError((err: HttpResponse<object>) => this.handleError(err.status)));
  }

  getBoard(sprintId: number): Observable<Board> {
    return this.http.get<Board>(
      `${environment.endpoint}/tasks/board`,
      {
        params: {sprint_id: String(sprintId)}
      }).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  saveBoard(board: Board, projectId: number, sprintId: number): Observable<boolean> {
    return this.http.put(
      `${environment.endpoint}/tasks/board`,
      {
        board,
        project_id: projectId,
        sprint_id: sprintId
      }
    ).pipe(
      map(smth => true),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  createTask(task: Task): Observable<number> {
    return this.http.post<Task>(
      `${environment.endpoint}/tasks`,
      {
        ...task,
        tags: task.tags.map(tag => tag.id)
      },
      {observe: 'response'}).pipe(
      map(response => response.status),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(
      `${environment.endpoint}/tasks/${task.id}`,
      {
        ...task,
        tags: task.tags.map(tag => tag.id)
      },
    ).pipe(catchError((err: HttpResponse<object>) => this.handleError(err.status)));
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }
}
