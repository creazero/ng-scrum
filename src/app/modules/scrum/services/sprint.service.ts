import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Sprint } from '../models/sprint';
import { environment } from '../../../../environments/environment';
import { format } from 'date-fns';
import { ChartData } from '../models/chart-data';

@Injectable({
  providedIn: 'root'
})
export class SprintService {

  constructor(private http: HttpClient,
              private router: Router) { }

  createSprint(sprint: Sprint): Observable<boolean> {
    return this.http.post<Sprint>(
      `${environment.endpoint}/sprints`,
      sprint).pipe(
        map(response => !!response),
        catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getSprint(sprintId: string): Observable<Sprint> {
    return this.http.get<Sprint>(`${environment.endpoint}/sprints/${sprintId}`).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getSprints(projectId: string): Observable<Sprint[]> {
    return this.http.get<Sprint[]>(
      `${environment.endpoint}/sprints`,
      {
        params: { project_id: projectId }
      }).pipe(
        catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getOngoingSprint(projectId: string): Observable<Sprint> {
    return this.http.get<{ sprint: Sprint }>(
      `${environment.endpoint}/sprints/ongoing`,
      {
        params: {project_id: projectId}
      }).pipe(
      map(data => data.sprint),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  hasIntersection(startDate: Date, projectId: string): Observable<boolean> {
    return this.http.post<{ has_intersection: boolean }>(
      `${environment.endpoint}/sprints/intersection`,
      {startDate: format(startDate, 'YYYY-MM-DD'), projectId: +projectId}).pipe(
      map(data => data.has_intersection),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getChartData(sprintId: number): Observable<ChartData> {
    return this.http.get<ChartData>(
      `${environment.endpoint}/sprints/chart_data`,
      {
        params: { sprint_id: String(sprintId) }
      }).pipe(
        catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  updateSprint(sprint: Sprint): Observable<Sprint> {
    return this.http.put<Sprint>(`${environment.endpoint}/sprints/${sprint.id}`, sprint).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }
}
