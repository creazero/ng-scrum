import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Project } from '../models/project';
import { environment } from '../../../../environments/environment';
import { User } from '../../../models/user';

@Injectable()
export class ProjectsService {

  constructor(private http: HttpClient,
              private router: Router) { }

  createProject(project: Project): Observable<Project> {
    return this.http.post<Project>(
      `${environment.endpoint}/projects`,
      project
    ).pipe(
        catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  checkName(name: string): Observable<boolean> {
    return this.http.get<{unique: boolean}>(`${environment.endpoint}/projects/unique`, { params: { name } }).pipe(
      map(data => data.unique),
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  deleteProject(projectId: number): Observable<boolean> {
    return this.http.delete(`${environment.endpoint}/projects/${projectId}`, { observe: 'response' }).pipe(
      map(response => response.status === 200),
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  /**
   * Get a list of a projects created by current user.
   */
  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(environment.endpoint + '/projects').pipe(
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  getProject(projectId: string): Observable<Project> {
    return this.http.get<Project>(`${environment.endpoint}/projects/${projectId}`).pipe(
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  access(projectId: string, userId: number, op: 'give' | 'revoke'): Observable<User[]> {
    return this.http.post<User[]>(
      `${environment.endpoint}/projects/access`,
      { project_id: +projectId, user_id: userId, op }
    ).pipe(
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  /**
   * Updating `project` on server.
   * @param project changed project
   */
  updateProject(project: Project): Observable<Project> {
    return this.http.put<Project>(`${environment.endpoint}/projects/${project.id}`, project).pipe(
      catchError((err: HttpResponse<Project>) => this.handleError(err.status))
    );
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }
}
