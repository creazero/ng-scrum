import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';

import { Tag } from '../models/tag';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class TagService {

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private router: Router) { }

  createTag(tag: Tag): Observable<Tag> {
    return this.http.post<Tag>(`${environment.endpoint}/tags`, tag).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  deleteTag(tagId: number): Observable<number> {
    return this.http.delete(`${environment.endpoint}/tags/${tagId}`, { observe: 'response' }).pipe(
      map((response: HttpResponse<object>) => response.status),
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  getTags(projectId: string): Observable<Tag[]> {
    return this.http.get<Tag[]>(`${environment.endpoint}/tags`, { params: { project_id: projectId } }).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  updateTag(tag: Tag): Observable<Tag> {
    return this.http.put<Tag>(`${environment.endpoint}/tags/${tag.id}`, tag).pipe(
      catchError((err: HttpResponse<object>) => this.handleError(err.status))
    );
  }

  private handleError(status: number): Observable<null> {
    switch (status) {
      case 500:
        this.router.navigate(['/500']);
        break;
      case 404:
        this.router.navigate(['/404']);
        break;
      case 403:
        this.router.navigate(['/projects']);
        break;
      default:
        this.router.navigate(['/500']);
        break;
    }
    return of(null);
  }

  private openSnack(message: string): void {
    this.snackBar.open(message, 'Ок', {
      duration: 3000
    });
  }
}
