import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule, MatCardModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule,
  MatMenuModule, MatNativeDateModule, MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule, MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChartsModule } from 'ng2-charts';

import { ScrumRoutingModule } from './scrum-routing.module';
import { ProjectsPageComponent } from './containers/projects-page/projects-page.component';
import { IndexComponent } from './containers/index/index.component';
import { ProjectsService } from './services/projects.service';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectPageComponent } from './containers/project-page/project-page.component';
import { BacklogPageComponent } from './containers/backlog-page/backlog-page.component';
import { TaskService } from './services/task.service';
import { BacklogListComponent } from './components/backlog-list/backlog-list.component';
import { DateDistancePipe } from './pipes/date-distance.pipe';
import { NewTaskComponent } from './containers/new-task/new-task.component';
import { BoardComponent } from './components/board/board.component';
import { BoardPageComponent } from './containers/board-page/board-page.component';
import { BoardItemComponent } from './components/board-item/board-item.component';
import { SprintsPageComponent } from './containers/sprints-page/sprints-page.component';
import { SprintsListComponent } from './components/sprints-list/sprints-list.component';
import { NewSprintComponent } from './containers/new-sprint/new-sprint.component';
import { SprintTasksComponent } from './components/sprint-tasks/sprint-tasks.component';
import { AssignModalComponent } from './components/assign-modal/assign-modal.component';
import { UsersService } from './services/users.service';
import { ProjectSettingsPageComponent } from './containers/project-settings-page/project-settings-page.component';
import { SprintChartModalComponent } from './components/sprint-chart-modal/sprint-chart-modal.component';
import { NewProjectComponent } from './containers/new-project/new-project.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { TagModalComponent } from './components/tag-modal/tag-modal.component';
import { TagService } from './services/tag.service';
import { TagLineComponent } from './components/tag-line/tag-line.component';
import { MembersPageComponent } from './containers/members-page/members-page.component';
import { MemberListComponent } from './components/member-list/member-list.component';
import { AddMemberModalComponent } from './components/add-member-modal/add-member-modal.component';

@NgModule({
  declarations: [
    ProjectsPageComponent,
    IndexComponent,
    ProjectsListComponent,
    ProjectPageComponent,
    BacklogPageComponent,
    BacklogListComponent,
    DateDistancePipe,
    NewTaskComponent,
    BoardComponent,
    BoardPageComponent,
    BoardItemComponent,
    SprintsPageComponent,
    SprintsListComponent,
    NewSprintComponent,
    SprintTasksComponent,
    AssignModalComponent,
    ProjectSettingsPageComponent,
    SprintChartModalComponent,
    NewProjectComponent,
    ColorPickerComponent,
    TagModalComponent,
    TagLineComponent,
    MembersPageComponent,
    MemberListComponent,
    AddMemberModalComponent
  ],
  entryComponents: [
    AssignModalComponent,
    SprintChartModalComponent,
    TagModalComponent,
    AddMemberModalComponent
  ],
  imports: [
    CommonModule,
    ScrumRoutingModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatBadgeModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    DragDropModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatChipsModule,
    MatAutocompleteModule,
    ChartsModule
  ],
  providers: [ProjectsService, TaskService, UsersService, TagService]
})
export class ScrumModule { }
