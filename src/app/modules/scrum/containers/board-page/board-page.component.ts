import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { TaskService } from '../../services/task.service';
import { SprintService } from '../../services/sprint.service';
import { Sprint } from '../../models/sprint';
import { Board } from '../../models/board';

@Component({
  selector: 'app-board-page',
  templateUrl: './board-page.component.html',
  styleUrls: ['./board-page.component.css']
})
export class BoardPageComponent implements OnInit {

  board: Board;

  currentSprint: Sprint;

  private projectId: string;

  constructor(private taskService: TaskService,
              private sprintService: SprintService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.paramMap.pipe(
      switchMap(map => {
        this.projectId = map.get('id');
        return this.sprintService.getOngoingSprint(this.projectId);
      }),
      switchMap(sprint => {
        if (sprint) {
          this.currentSprint = sprint;
          return this.taskService.getBoard(sprint.id);
        } else {
          return of(null);
        }
      })
    ).subscribe(board => this.board = board);
  }

  onNewBoard(board: Board) {
    this.taskService.saveBoard(board, +this.projectId, this.currentSprint.id).subscribe();
  }
}
