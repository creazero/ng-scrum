import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';

import { switchMap } from 'rxjs/operators';

import { UsersService } from '../../services/users.service';
import { User } from '../../../../models/user';
import { AddMemberModalComponent } from '../../components/add-member-modal/add-member-modal.component';
import { UsersDialogData } from '../../models/users-dialog-data';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'scr-members-page',
  templateUrl: './members-page.component.html',
  styleUrls: ['./members-page.component.css']
})
export class MembersPageComponent implements OnInit {

  public projectUsers: User[] = [];
  private allUsers: User[] = [];

  constructor(private usersService: UsersService,
              private route: ActivatedRoute,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar,
              private projectsService: ProjectsService) { }

  ngOnInit() {
    this.route.parent.paramMap.pipe(
      switchMap(paramMap => this.usersService.getProjectUsers(paramMap.get('id')))
    ).subscribe(users => this.projectUsers = users);

    this.usersService.getAllUsers().subscribe(users => this.allUsers = users);
  }

  public onAddUser(): void {
    if (isUsersArrayEqual(this.projectUsers, this.allUsers)) {
      this.snackBar.open(
        'В системе не существует пользователей, которые не добавлены в текущий проект',
        'Ок',
        { duration: 3000 });
      return;
    }
    const dialogRef = this.matDialog.open(AddMemberModalComponent, {
      data: {
        allUsers: this.allUsers,
        projectUsers: this.projectUsers
      } as UsersDialogData
    });

    dialogRef.afterClosed().subscribe((userId: number) => {
      if (userId) {
        const projectId = this.route.parent.snapshot.paramMap.get('id');
        this.projectsService.access(projectId, userId, 'give')
          .subscribe(users => {
            if (users) {
              this.projectUsers = users;
            }
          });
      }
    });
  }

  public onDeleteUser(userId: number): void {
    const projectId = this.route.parent.snapshot.paramMap.get('id');
    this.projectsService.access(projectId, userId, 'revoke')
      .subscribe(users => {
        if (users) {
          this.projectUsers = users;
        }
      });
  }

}

function isUsersArrayEqual(a1: User[], a2: User[]): boolean {
  if (a1.length !== a2.length) {
    return false;
  }

  for (let i = 0; i < a1.length; i++) {
    if (a1[i].id !== a2[i].id) {
      return false;
    }
  }
  return true;
}
