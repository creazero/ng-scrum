import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';

import { TaskService } from '../../services/task.service';
import { Task, TaskState } from '../../models/task';

@Component({
  templateUrl: './backlog-page.component.html',
  styleUrls: ['./backlog-page.component.css']
})
export class BacklogPageComponent implements OnInit, OnDestroy {

  activeTasks: Task[] = [];
  inProcessTasks: Task[] = [];
  doneTasks: Task[] = [];

  /**
   * A list of tasks
   */
  private tasks: Task[];

  private routeSub: Subscription;

  constructor(private route: ActivatedRoute, private taskService: TaskService) { }

  ngOnInit() {
    this.routeSub = this.route.parent.paramMap
      .subscribe(map => this.retrieveTasks(map.get('id')));
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  private retrieveTasks(projectId: string): void {
    this.taskService.getTasksFromProject(projectId)
      .subscribe(tasks => {
        this.tasks = tasks;
        this.tasks.forEach(task => {
          if (task.state === TaskState.DONE) {
            this.doneTasks = [...this.doneTasks, task];
          } else if (task.state === null) {
            this.activeTasks = [...this.activeTasks, task];
          } else {
            this.inProcessTasks = [...this.inProcessTasks, task];
          }
        });
      });
  }
}
