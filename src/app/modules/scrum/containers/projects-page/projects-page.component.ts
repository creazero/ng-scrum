import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';

@Component({
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css']
})
export class ProjectsPageComponent implements OnInit {

  projects: Project[];

  constructor(private projectsService: ProjectsService, private snackBar: MatSnackBar) { }

  /**
   * Get all favorite projects
   */
  get favoriteProjects(): Project[] {
    return this.projects ? this.projects.filter(project => project.isFavorite) : [];
  }

  ngOnInit() {
    this.projectsService.getProjects()
      .subscribe(projects => this.projects = projects);
  }

  /**
   * Function checks whether user has any favorite projects.
   */
  hasFavorite(): boolean {
    return this.projects ? this.projects.some(project => project.isFavorite) : false;
  }

  /**
   * Handling an event of a user's click on the star
   * @param project project with changed state
   */
  onFavorite(project: Project) {
    // making a copy of the project
    const copy = { ...project };
    // and changing its isFavorite field
    copy.isFavorite = !project.isFavorite;
    this.projectsService.updateProject(copy)
      .subscribe(isSuccess => {
        if (!isSuccess) {
          // showing an error message
          this.snackBar.open('Ошибка при сохранении проекта');
        } else {
          project.isFavorite = !project.isFavorite;
        }
      });
  }
}
