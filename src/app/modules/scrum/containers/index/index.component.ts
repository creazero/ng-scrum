import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service';
import { User } from '../../../../models/user';

@Component({
  selector: 'app-index',
  styleUrls: ['./index.component.css'],
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
  /**
   * Текущий пользователь
   */
  user: User;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.getUser()
      .subscribe(
        user => this.user = user,
        (error: Response) => {
          // срок действия токена истек
          if (error.status === 401 || error.status === 403) {
            localStorage.removeItem('token');
            this.router.navigate(['/auth/login']);
          }
        }
      );
  }

  /**
   * Клик на кнопку логаута
   */
  onLogout(): void {
    this.authService.logout();
    // редирект на страницу логина
    this.router.navigate(['/auth/login']);
  }
}
