import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { addMinutes, format } from 'date-fns';

import { SprintService } from '../../services/sprint.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Sprint } from '../../models/sprint';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';

@Component({
  selector: 'app-new-sprint',
  templateUrl: './new-sprint.component.html',
  styleUrls: ['./new-sprint.component.css']
})
export class NewSprintComponent implements OnInit {

  /**
   * All tasks from the project
   */
  allTasks: Task[] = [];
  /**
   * Sprint form
   */
  form: FormGroup;

  /**
   * A list of sprint's tasks
   */
  public sprintTasks: Task[] = [];
  public dirtyHack = false;
  private projectId: string;
  private sprint: Sprint;

  constructor(private fb: FormBuilder,
              private sprintService: SprintService,
              private taskService: TaskService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  public ngOnInit(): void {
    this.projectId = this.route.parent.snapshot.paramMap.get('id');

    this.sprint = { startDate: null, projectId: +this.projectId, tasks: [] };

    // fetch tasks (nothing can go wrong, right?)
    this.taskService.getTasksFromProject(this.projectId)
      .subscribe(tasks =>
        this.allTasks = tasks.filter(task => task.sprintId === null)
      );

    this.route.paramMap.pipe(
      switchMap(paramMap => {
        const sprintId = paramMap.get('sprint_id');
        return sprintId ? this.sprintService.getSprint(sprintId) : of(null);
      })
    ).subscribe(sprint => {
      if (sprint) {
        this.sprint = sprint;
        this.form.patchValue({
          startDate: sprint.startDate
        });
        this.sprintTasks = sprint.tasks;
      }
    });
    this.initForm();
  }

  public onNewTasks(tasks: Task[]): void {
    this.sprintTasks = tasks;
    this.dirtyHack = true;
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    const startDate: Date = new Date(this.form.value.startDate);
    if (!this.sprint.id) {
      const sprint: Sprint = {
        startDate: format(addMinutes(startDate, -startDate.getTimezoneOffset()), 'YYYY-MM-DD'),
        projectId: this.sprint.projectId,
        tasks: this.sprintTasks.map(task => task.id)
      };
      this.sprintService.createSprint(sprint).subscribe(isSuccess => {
        if (isSuccess) {
          this.router.navigate(['../sprints'], {relativeTo: this.route});
        }
      });
    } else {
      this.sprint.startDate = format(addMinutes(startDate, -startDate.getTimezoneOffset()), 'YYYY-MM-DD');
      this.sprint.tasks = this.sprintTasks.map(task => task.id);
      this.sprintService.updateSprint(this.sprint).subscribe(sprint => {
        if (sprint) {
          this.router.navigate(['../../sprints'], {relativeTo: this.route});
        }
      });
    }
  }

  /**
   * Initialize form
   */
  private initForm(): void {
    this.form = this.fb.group({
      startDate: this.fb.control(
        this.sprint.startDate,
        [Validators.required],
        [this.intersectionValidator.bind(this)]
      )
    });
  }

  private intersectionValidator(control: FormControl): Observable<ValidationErrors> {
    let startDate: Date = new Date(control.value);
    if (!startDate) {
      return null;
    }
    startDate = addMinutes(startDate, -startDate.getTimezoneOffset());
    return this.sprintService.hasIntersection(startDate, this.projectId).pipe(
      map(hasIntersection => {
        const strDate = format(startDate, 'YYYY-MM-DD');
        return hasIntersection && this.sprint.startDate !== strDate ? { intersection: true } : null;
      }),
      catchError(() => of(null))
    );
  }
}
