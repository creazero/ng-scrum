import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';

import { map, switchMap } from 'rxjs/operators';

import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';
import { TagModalComponent } from '../../components/tag-modal/tag-modal.component';
import { TagDialogData } from '../../models/tag-dialog-data';
import { Tag } from '../../models/tag';
import { TagService } from '../../services/tag.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'scr-project-settings-page',
  templateUrl: './project-settings-page.component.html',
  styleUrls: ['./project-settings-page.component.css']
})
export class ProjectSettingsPageComponent implements OnInit {

  public currentProject: Project;
  public form: FormGroup;

  constructor(private projectService: ProjectsService,
              private tagService: TagService,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  public ngOnInit(): void {
    this.route.parent.paramMap.pipe(
      switchMap(paramMap => this.projectService.getProject(paramMap.get('id')))
    ).subscribe(project => {
      this.currentProject = project;
      this.initForm();
    });
  }

  public onDelete(): void {
    this.projectService.deleteProject(this.currentProject.id)
      .subscribe(success => {
        if (success) {
          this.router.navigate(['/projects']);
        } else {
          this.snackBar.open(
            'Во время удаления проекта возникла ошибка. Попробуйте позже.',
            'Ок',
            { duration: 3000 }
          );
        }
      });
  }

  public onNewTag(): void {
    const dialogRef = this.dialog.open(TagModalComponent, {
      data: {
        tag: null,
        projectId: this.currentProject.id
      } as TagDialogData
    });

    dialogRef.afterClosed().subscribe((tag: Tag) => {
      if (tag) {
        this.tagService.createTag(tag).subscribe((newTag: Tag) => {
          if (newTag) {
            this.currentProject.tags.push(newTag);
          }
        });
      }
    });
  }

  public onTagClick(tag: Tag): void {
    const dialogRef = this.dialog.open(TagModalComponent, {
      data: {
        tag,
        projectId: this.currentProject.id
      } as TagDialogData
    });

    dialogRef.afterClosed().subscribe((updatedTag: Tag) => {
      if (updatedTag) {
        this.tagService.updateTag(updatedTag).subscribe((serverTag: Tag) => {
          if (serverTag) {
            const index = this.currentProject.tags.findIndex(t => t.id === serverTag.id);
            if (index) {
              this.currentProject.tags[index] = serverTag;
            }
          }
        });
      }
    });
  }

  public onTagRemoval(tag: Tag): void {
    this.tagService.deleteTag(tag.id)
      .subscribe((status: number) => {
        if (status === 200) {
          const index = this.currentProject.tags.findIndex((fTag: Tag) => fTag.id === tag.id);
          this.currentProject.tags.splice(index, 1);
        }
      });
  }

  public onSubmit(): void {
    if (!this.form.valid) { return; }
    this.currentProject = {
      ...this.currentProject,
      name: this.form.value.name,
      description: this.form.value.description,
      sprintLength: this.form.value.sprintLength,
      color: this.form.value.color,
    };
    this.projectService.updateProject(this.currentProject)
      .subscribe(project => {
        if (project) {
          this.form.markAsPristine();
        }
      });
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: this.fb.control(this.currentProject.name, Validators.required, this.projectNameValidator.bind(this)),
      description: this.fb.control(this.currentProject.description),
      sprintLength: this.fb.control(
        this.currentProject.sprintLength,
        [Validators.required, Validators.min(1)]
      ),
      color: this.fb.control(this.currentProject.color, Validators.required)
    });
  }

  private projectNameValidator(control: FormControl): Observable<ValidationErrors> {
    const value: string = control.value;
    if (!value) {
      return of(null);
    }
    return this.projectService.checkName(value).pipe(
      map(isUnique => isUnique || value === this.currentProject.name ? null : { integrity: true })
    );
  }
}
