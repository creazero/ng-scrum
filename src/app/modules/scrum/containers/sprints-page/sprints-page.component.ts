import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { SprintService } from '../../services/sprint.service';
import { Sprint } from '../../models/sprint';

@Component({
  templateUrl: './sprints-page.component.html',
  styleUrls: ['./sprints-page.component.css']
})
export class SprintsPageComponent implements OnInit, OnDestroy {

  sprints: Sprint[];

  private routeSub: Subscription;

  constructor(private sprintService: SprintService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.routeSub = this.route.parent.paramMap.pipe(
      switchMap(map => this.sprintService.getSprints(map.get('id')))
    ).subscribe(sprints => this.sprints = sprints);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
