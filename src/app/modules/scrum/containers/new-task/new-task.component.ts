import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Tag } from '../../models/tag';
import { TagService } from '../../services/tag.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {

  form: FormGroup;
  task: Task = {
    description: '',
    name: '',
    priority: 0,
    projectId: null,
    weight: 0,
    tags: [],
  };
  public tags: Tag[] = [];

  private projectId: string;

  constructor(private fb: FormBuilder,
              private taskService: TaskService,
              private route: ActivatedRoute,
              private router: Router,
              private tagService: TagService) { }

  ngOnInit() {
    this.projectId = this.route.parent.snapshot.paramMap.get('id');
    this.route.paramMap.pipe(
      switchMap(map => {
        const taskId: string = map.get('task_id');
        if (taskId) {
          return this.taskService.getTask(map.get('task_id'));
        } else {
          return of(null);
        }
      })
    )
      .subscribe(task => {
        if (task) {
          this.task = task;
        }
        this.initForm();
      });

    this.tagService.getTags(this.projectId)
      .subscribe(tags => {
        if (tags !== null) {
          this.tags = tags;
        }
      });
  }

  public onDelete(): void {
    if (!this.task.id) { return; }
    this.taskService.deleteTask(this.task.id)
      .subscribe(success => {
        if (success) {
          this.router.navigate(['../../backlog'], {relativeTo: this.route});
        }
      });
  }

  onSubmit(): void {
    if (!this.form.valid) { return; }
    const task: Task = {
      id: this.task.id,
      name: this.form.value.name,
      description: this.form.value.description,
      weight: this.form.value.weight,
      priority: this.form.value.priority,
      projectId: +this.projectId,
      tags: this.task.tags
    };
    if (!this.task.id) {
      this.taskService.createTask(task)
        .subscribe(status => {
          if (status === 201) {
            this.router.navigate(['../backlog'], {relativeTo: this.route});
          }
        });
    } else {
      this.taskService.updateTask(task).subscribe(newTask => {
        if (newTask) {
          this.router.navigate(['../../backlog'], {relativeTo: this.route});
        }
      });
    }
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: this.fb.control(this.task.name, Validators.required),
      description: this.fb.control(this.task.description),
      weight: this.fb.control(this.task.weight, Validators.min(0)),
      priority: this.fb.control(this.task.priority, Validators.required),
      tags: this.fb.control(this.task.tags)
    });
  }

  onTagSelect(event: MatAutocompleteSelectedEvent) {
    const tag: Tag = this.tags.find(fTag => fTag.id === event.option.value);
    if (tag) {
      this.task.tags.push(tag);
    }
  }
}
