import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'scr-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  public form: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private projectsService: ProjectsService) { }

  public ngOnInit(): void {
    this.initForm();
  }

  public onSubmit(): void {
    if (!this.form.valid) { return; }
    const newProject: Project = {
      sprintLength: this.form.value.sprintLength,
      name: this.form.value.name,
      description: this.form.value.description,
      color: this.form.value.color
    };
    this.projectsService.createProject(newProject)
      .subscribe(project => {
        if (project) {
          this.router.navigate(['/projects', project.id]);
        }
      });
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: this.fb.control('', Validators.required, this.projectNameValidator.bind(this)),
      description: this.fb.control(''),
      sprintLength: this.fb.control(
        2,
        [Validators.required, Validators.min(1)]
      ),
      color: this.fb.control('#3F51B5', Validators.required)
    });
  }

  private projectNameValidator(control: FormControl): Observable<ValidationErrors> {
    const value: string = control.value;
    if (!value) {
      return of(null);
    }
    return this.projectsService.checkName(value).pipe(
      map(isUnique => isUnique ? null : { integrity: true })
    );
  }
}
