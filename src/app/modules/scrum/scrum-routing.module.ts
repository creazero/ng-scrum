import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './containers/index/index.component';
import { ProjectsPageComponent } from './containers/projects-page/projects-page.component';
import { AuthGuard } from './guards/auth.guard';
import { ProjectPageComponent } from './containers/project-page/project-page.component';
import { BacklogPageComponent } from './containers/backlog-page/backlog-page.component';
import { NewTaskComponent } from './containers/new-task/new-task.component';
import { BoardPageComponent } from './containers/board-page/board-page.component';
import { SprintsPageComponent } from './containers/sprints-page/sprints-page.component';
import { NewSprintComponent } from './containers/new-sprint/new-sprint.component';
import { ProjectSettingsPageComponent } from './containers/project-settings-page/project-settings-page.component';
import { NewProjectComponent } from './containers/new-project/new-project.component';
import { MembersPageComponent } from './containers/members-page/members-page.component';

const routes: Routes = [
  {
    canActivate: [AuthGuard],
    children: [
      {
        component: ProjectsPageComponent,
        path: '',
      },
      {
        component: NewProjectComponent,
        path: 'create_project'
      },
      {
        children: [
          {
            component: BoardPageComponent,
            path: ''
          },
          {
            component: BacklogPageComponent,
            path: 'backlog'
          },
          {
            component: NewTaskComponent,
            path: 'backlog/:task_id'
          },
          {
            component: NewTaskComponent,
            path: 'new_task'
          },
          {
            component: NewSprintComponent,
            path: 'sprints/:sprint_id'
          },
          {
            component: SprintsPageComponent,
            path: 'sprints'
          },
          {
            component: NewSprintComponent,
            path: 'new_sprint'
          },
          {
            component: ProjectSettingsPageComponent,
            path: 'settings'
          },
          {
            component: MembersPageComponent,
            path: 'members'
          }
        ],
        component: ProjectPageComponent,
        path: ':id',
      }
    ],
    component: IndexComponent,
    path: '',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScrumRoutingModule { }
