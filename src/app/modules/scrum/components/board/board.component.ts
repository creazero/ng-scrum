import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { Board } from '../../models/board';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Task } from '../../models/task';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent {
  /**
   * A list of tasks for board
   */
  @Input() board: Board;
  @Output() boardUpdate = new EventEmitter<Board>();

  drop(event: CdkDragDrop<Task[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
    this.boardUpdate.emit(this.board);
  }
}
