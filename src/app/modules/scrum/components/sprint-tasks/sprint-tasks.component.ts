import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

import { Task } from '../../models/task';

@Component({
  selector: 'scr-sprint-tasks',
  templateUrl: './sprint-tasks.component.html',
  styleUrls: ['./sprint-tasks.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SprintTasksComponent {
  /**
   * All tasks from the current project
   */
  @Input() allTasks: Task[] = [];
  /**
   * Tasks from the current sprint
   */
  @Input() sprintTasks: Task[] = [];

  /**
   * An event for emitting a new list of tasks
   */
  @Output() sprintTasksEvent = new EventEmitter<Task[]>();

  constructor(private snackBar: MatSnackBar) {}

  /**
   * A function for handling a drag'n'drop event.
   * @param event an info about event, containing previous/current container and additional information
   * about data.
   */
  drop(event: CdkDragDrop<Task[]>): void {
    const task = event.previousContainer.data[event.previousIndex] as Task;
    if (task.weight === 0) {
      this.snackBar.open(
        'Задача с нулевой трудоемкостью не может быть добавлена в спринт',
        'Ок',
        { duration: 3000 }
      );
      return;
    }
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
    this.sprintTasksEvent.emit(this.sprintTasks);
  }
}
