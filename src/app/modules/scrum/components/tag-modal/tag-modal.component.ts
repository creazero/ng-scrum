import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { TagDialogData } from '../../models/tag-dialog-data';

@Component({
  selector: 'scr-tag-modal',
  templateUrl: './tag-modal.component.html',
  styleUrls: ['./tag-modal.component.css']
})
export class TagModalComponent implements OnInit {

  public form: FormGroup;

  constructor(public dialogRef: MatDialogRef<TagModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TagDialogData,
              private fb: FormBuilder) { }

  public ngOnInit() {
    if (!this.data.tag) {
      this.data.tag = {
        color: '',
        name: '',
        projectId: this.data.projectId
      };
    }
    this.initForm();
  }

  public onCloseClick(): void {
    this.dialogRef.close();
  }

  public onSaveClick(): void {
    if (!this.form.valid) { return; }
    this.data.tag.name = this.form.value.name;
    this.data.tag.color = this.form.value.color;
    this.dialogRef.close(this.data.tag);
  }

  private initForm(): void {
    const tag = this.data.tag;
    this.form = this.fb.group({
      name: this.fb.control(tag.name, Validators.required),
      color: this.fb.control(tag.color, Validators.required)
    });
  }
}
