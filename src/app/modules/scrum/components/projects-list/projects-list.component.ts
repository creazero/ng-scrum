import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { Project } from '../../models/project';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent {
  /**
   * A list of visible projects
   */
  @Input() projects: Project[];
  /**
   * An event for notifying about a click on the star
   */
  @Output() favoriteEvent = new EventEmitter<Project>();
}
