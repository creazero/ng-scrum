import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Sprint } from '../../models/sprint';
import { MatDialog } from '@angular/material';
import { SprintChartModalComponent } from '../sprint-chart-modal/sprint-chart-modal.component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-sprints-list',
  templateUrl: './sprints-list.component.html',
  styleUrls: ['./sprints-list.component.css']
})
export class SprintsListComponent {
  @Input() activeSprint: Sprint;
  @Input() sprints: Sprint[] = [];

  constructor(private dialog: MatDialog) {}

  public onShowChart(sprintId: number, event: MouseEvent): false {
    this.dialog.open(SprintChartModalComponent, {
      data: sprintId
    });
    event.stopPropagation();
    return false;
  }
}
