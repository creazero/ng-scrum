import { ChangeDetectionStrategy, Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ColorPickerComponent),
      multi: true
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor {

  public color = '#3F51B5';
  /**
   * Предопределенные цвета
   */
  public readonly predefinedColors: string[] = [
    '#9C27B0', '#673AB7', '#3F51B5', '#009688', '#4CAF50',
    '#607D8B', '#FFC107'
  ];

  public onChange: (color: string) => void;
  public onTouched: (color: string) => void;

  public onPick(color: string): void {
    this.color = color;
    this.onChange(color);
  }

  public registerOnChange(fn: (color: string) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: (color: string) => void): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
  }

  public writeValue(newColor: string): void {
    if (!newColor.length) {
      this.color = '#3F51B5';
    } else {
      this.color = newColor;
    }
  }
}
