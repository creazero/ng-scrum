import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { UsersService } from '../../services/users.service';
import { User } from '../../../../models/user';
import { DialogData } from '../../models/dialog-data';

@Component({
  selector: 'scr-assign-modal',
  templateUrl: './assign-modal.component.html',
  styleUrls: ['./assign-modal.component.css']
})
export class AssignModalComponent implements OnInit {

  users: User[];

  constructor(public dialogRef: MatDialogRef<AssignModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private usersService: UsersService) {}

  public ngOnInit(): void {
    this.usersService.getProjectUsers(String(this.data.projectId))
      .subscribe(users => this.users = users);
  }

  public onCloseClick(): void {
    this.dialogRef.close();
  }
}
