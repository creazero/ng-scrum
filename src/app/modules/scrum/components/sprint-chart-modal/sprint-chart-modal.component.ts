import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { SprintService } from '../../services/sprint.service';
import { ChartData } from '../../models/chart-data';
import { ChartOptions } from 'chart.js';
import { Color } from 'ng2-charts';

@Component({
  selector: 'app-sprint-chart-modal',
  templateUrl: './sprint-chart-modal.component.html',
  styleUrls: ['./sprint-chart-modal.component.css']
})
export class SprintChartModalComponent implements OnInit {

  public data: ChartData;

  public readonly chartOptions: ChartOptions = {
    elements: {
      line: {
        tension: 0
      }
    }
  };

  public readonly lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0)',
      borderColor: 'rgb(255,103,252)'
    }
  ];

  constructor(public dialogRef: MatDialogRef<SprintChartModalComponent>,
              @Inject(MAT_DIALOG_DATA) public sprintId: number,
              private sprintService: SprintService) { }

  ngOnInit() {
    this.sprintService.getChartData(this.sprintId)
      .subscribe(data => this.data = data);
  }

  public onCloseClick(): void {
    this.dialogRef.close();
  }

}
