import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Task } from '../../models/task';
import { TaskService } from '../../services/task.service';
import { AssignModalComponent } from '../assign-modal/assign-modal.component';
import { DialogData } from '../../models/dialog-data';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { User } from '../../../../models/user';
import { of } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-board-item',
  templateUrl: './board-item.component.html',
  styleUrls: ['./board-item.component.css']
})
export class BoardItemComponent {
  @Input() task: Task;
  @Input() hasActions = true;

  private readonly projectId: number;

  constructor(private taskService: TaskService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
    this.projectId = +this.route.snapshot.params.id;
  }

  public openAssignModal(): void {
    const dialogRef = this.dialog.open(AssignModalComponent, {
      data: {
        projectId: this.projectId,
        selectedUser: this.task.assigneeId
      } as DialogData,
      minWidth: '450px',
      width: '23vw'
    });

    dialogRef.afterClosed().pipe(
      switchMap((userId: number) => {
        if (userId !== null && userId !== undefined) {
          return this.taskService.assign(this.task, userId);
        } else {
          return of(null);
        }
      })
    ).subscribe((assignee: User) => {
      if (assignee) {
        this.task.assigneeId = assignee.id;
        this.task.assignee = assignee;
      }
    });
  }
}
