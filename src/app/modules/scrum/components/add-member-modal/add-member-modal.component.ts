import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { User } from '../../../../models/user';
import { UsersDialogData } from '../../models/users-dialog-data';

@Component({
  selector: 'scr-add-member-modal',
  templateUrl: './add-member-modal.component.html',
  styleUrls: ['./add-member-modal.component.css']
})
export class AddMemberModalComponent implements OnInit {

  public users: User[] = [];
  public selectedUser: number = null;

  constructor(private dialogRef: MatDialogRef<AddMemberModalComponent>,
              @Inject(MAT_DIALOG_DATA) private data: UsersDialogData) { }

  public ngOnInit(): void {
    const projectUserIds = this.data.projectUsers.map(user => user.id);
    this.users = this.data.allUsers.filter(user => !projectUserIds.includes(user.id));
  }

  public onCloseClick(): void {
    this.dialogRef.close(null);
  }
}
