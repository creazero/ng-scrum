import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Tag } from '../../models/tag';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-tag-line',
  templateUrl: './tag-line.component.html',
  styleUrls: ['./tag-line.component.css']
})
export class TagLineComponent {
  @Input() tags: Tag[] = [];
}
