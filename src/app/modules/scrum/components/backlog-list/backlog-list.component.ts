import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Task } from '../../models/task';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'scr-backlog-list',
  templateUrl: './backlog-list.component.html',
  styleUrls: ['./backlog-list.component.css']
})
export class BacklogListComponent {

  /**
   * A list of tasks
   */
  @Input() tasks: Task[];
}
