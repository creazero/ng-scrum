import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { User } from '../../../../models/user';

@Component({
  selector: 'scr-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MemberListComponent {
  @Input() public users: User[] = [];
  @Output() public userRevoke = new EventEmitter<number>();
}
