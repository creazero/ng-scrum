import { Task } from './task';

export interface Sprint {
  id?: number;
  startDate: string;
  projectId: number;
  endDate?: string;
  tasks?: any[];
}
