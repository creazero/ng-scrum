import { Tag } from './tag';

export interface TagDialogData {
  tag: Tag;
  projectId: number;
}
