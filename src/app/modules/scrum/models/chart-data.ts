import { ChartDataSets } from 'chart.js';

export interface ChartData {
  data: ChartDataSets[];
  labels: string[];
}
