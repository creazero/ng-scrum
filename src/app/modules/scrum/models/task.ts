import { User } from '../../../models/user';
import { Tag } from './tag';

export enum TaskState {
  TODO = 'todo',
  IN_PROCESS = 'inProcess',
  TESTING = 'testing',
  DONE = 'done'
}

export interface Task {
  assigneeId?: number;
  assignee?: User;
  createdAt?: Date;
  creator?: User;
  description: string;
  id?: number;
  name: string;
  projectId: number;
  sprintId?: number;
  weight: number;
  priority: number;
  state?: TaskState;
  tags?: Tag[];
}
