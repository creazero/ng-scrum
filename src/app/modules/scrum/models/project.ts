import { Tag } from './tag';

export interface Project {
  id?: number;
  name: string;
  description: string;
  color: string;
  createdAt?: string;
  creatorId?: number;
  isFavorite?: boolean;
  sprintLength: number;
  tags?: Tag[];
}
