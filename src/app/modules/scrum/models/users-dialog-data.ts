import { User } from '../../../models/user';

export interface UsersDialogData {
  projectUsers: User[];
  allUsers: User[];
}
