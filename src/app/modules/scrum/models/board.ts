import { Task } from './task';

export interface Board {
  todo: Task[];
  inProcess: Task[];
  testing: Task[];
  done: Task[];
}
