import { Pipe, PipeTransform } from '@angular/core';

import { addMinutes, distanceInWordsToNow } from 'date-fns';
import * as ruLocale from 'date-fns/locale/ru';

@Pipe({
  name: 'dateDistance'
})
export class DateDistancePipe implements PipeTransform {

  transform(value: Date): string {
    const timezoneDate = addMinutes(value, -new Date(value).getTimezoneOffset());
    return distanceInWordsToNow(timezoneDate, { addSuffix: true, locale: ruLocale });
  }

}
